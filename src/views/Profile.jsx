import { Box, Typography, Button, Card } from "@mui/material";
import { useKeycloak } from "@react-keycloak/web";
import UpdateProfilePictureModal from "../components/UpdateProfilePictureModal/UpdateProfilePictureModal";
import { useState } from "react";
import UpdateMeasurementsModal from "../components/UpdateMeasurementsModal/UpdateMeasurementsModal";
import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const Profile = () => {
    const { keycloak } = useKeycloak();
    const [state, setState] = useState('');
    const [buttonPressed, setButtonPressed] = useState(false);
    const user = JSON.parse(localStorage.getItem("user"));

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });
    const [open, setOpen] = React.useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };


    let profilePictureUrl;
    if (user.profilePicture) {
        profilePictureUrl = user.profilePicture;
    } else {
        profilePictureUrl = "https://i.pinimg.com/736x/3d/cd/4a/3dcd4af5bc9e06d36305984730ab7888.jpg"
    }

    const handleLogout = () => {
        localStorage.clear();
        keycloak.logout();
    }

    const contributorRequest = () => {
        setOpen(true)
        setButtonPressed(true)

        // Update localstorage
        user.contributor = true;
        localStorage.setItem("user", JSON.stringify(user))

        // Patch the user in the database
        // Set the Contributor request in database to true
        const patchUser = async () => {

            const patchRequestOptions = {
                method: "PATCH",
                headers: {
                    "Content-Type": "application/json",

                },
                body: JSON.stringify({
                    'contributor': user.contributor
                }),
            }
            // HTTP Patch the user
            fetch("https://apimefit.azurewebsites.net/api/Accounts/" + user.id, patchRequestOptions)

        }
        patchUser();
    }

    return (
        <>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <h1>Profile</h1>
                <Button variant="contained" sx={{ height: 40 }} onClick={() => handleLogout()}>Log out</Button>
            </Box>
            <Card sx={{ pl: 5, pr: 10, pt: 3 }}>
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column', p: 2, width: '100%', mb: 3 }}>
                        <Box sx={{ pb: 2, borderBottom: 1, borderColor: '#1976d2' }}>
                            <Typography variant="h3">{user.firstname} {user.lastname}</Typography>
                            <Typography variant="h5" color="gray">{user.email}</Typography>
                        </Box>
                        <Box sx={{ mt: 5, height: 220, mb: 4, display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                            <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                                <Typography variant="h4" sx={{ mb: 'auto', mt: 2 }}>
                                    Measurements
                                </Typography>
                                <UpdateMeasurementsModal updateState={setState} />
                            </Box>
                            <Box sx={{ mb: 1 }}>
                                <Typography variant="h5">
                                    Height
                                </Typography>
                                <Typography variant="h6">{user.height} cm</Typography>
                            </Box>
                            <Box>
                                <Typography variant="h5">
                                    Weight
                                </Typography>
                                <Typography variant="h6">{user.weight} kg</Typography>
                            </Box>
                        </Box>
                    </Box>
                    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', ml: 5 }}>
                        <img src={profilePictureUrl} alt="userpicture" style={{ width: 350, height: 350, borderRadius: 360, objectFit: 'cover' }} />
                        <UpdateProfilePictureModal updateState={setState} />
                    </Box>
                </Box>
                {!user.contributor === true &&
                    <Box mb={5}>
                        {!buttonPressed &&
                            <Button variant="contained" sx={{ height: 40 }} onClick={() => contributorRequest()}>Send a request to be a Contributor</Button>
                        }
                        {buttonPressed &&
                            <Button variant="contained" sx={{ height: 40 }} onClick={() => contributorRequest()} disabled>Send a request to be a Contributor</Button>
                        }
                    </Box>
                }
                <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                        Contributor request sent
                    </Alert>
                </Snackbar>
            </Card>
        </>
    )
}
export default Profile;