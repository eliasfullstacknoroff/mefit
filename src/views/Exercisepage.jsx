import '../App.css';
import { Grid, Typography, CircularProgress, Card, Box } from "@mui/material";
import exerciseFuncs from "../utils/ExerciseContext"
//Asd

const Exercisepage = () => {

    const exerciseId = window.location.pathname.match(/\d+/)[0];
    const data = exerciseFuncs.FetchExercise(exerciseId);


    if (data === undefined) {
        return <>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '80vh' }}
            >
                <CircularProgress />
            </Grid>
        </>
    }

    return (

        <>
            <h1>{data.name}</h1>
            <img src={data.image} alt="exercise.pic" />

            <Box sx={{ alignItems: 'center', mb: '2rem', mt: '1rem' }}>
                <Typography variant="h5">Description: </Typography>
                {data.description}
            </Box>
        </>
    )
}
export default Exercisepage;