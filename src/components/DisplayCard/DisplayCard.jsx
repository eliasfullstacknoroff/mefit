import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { NavLink } from 'react-router-dom';
import AddWorkoutToGoal from '../AddWorkoutToGoal/AddWorkoutToGoal';
import AddProgramToGoal from '../AddWorkoutToGoal/AddProgramToGoal';

//Asd
const DisplayCard = ({ element, type, id }) => {
  function capitalize(s) {
    return s[0].toUpperCase() + s.slice(1);
  }
  return (
    <>
      <Card sx={{ maxWidth: 360, maxHeight: 360 }}>
        <CardActionArea component={NavLink} to={{ pathname: `/${type}/${element.id}` }}>
          <CardMedia
            component="img"
            height="180"
            image={element.image ? element.image : "https://aestheticmedicalpractitioner.com.au/wp-content/uploads/2021/06/no-image.jpg"}
            alt={element.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {element.name ? capitalize(element.name) : "No title provided"}
            </Typography>

            <Typography variant="body2" color="text.secondary" sx={{
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              WebkitLineClamp: '3',
              WebkitBoxOrient: 'vertical',
            }}>
              {element.description ? capitalize(element.description) : " "}
            </Typography>

          </CardContent>
        </CardActionArea>
        {type === "workout" &&
          <AddWorkoutToGoal
            WorkoutId={id + 1}
          ></AddWorkoutToGoal>
        }
        {type === "program" &&
          <AddProgramToGoal
            ProgramId={id + 1}
          ></AddProgramToGoal>
        }
      </Card>
    </>
  );
}
export default DisplayCard;
