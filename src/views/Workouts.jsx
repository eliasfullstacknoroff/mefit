import DisplayCard from "../components/DisplayCard/DisplayCard";
import '../App.css';
import { Grid, Box } from "@mui/material";
import workoutFuncs from "../utils/WorkoutContext";
import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import AddWorkoutToGoal from "../components/AddWorkoutToGoal/AddWorkoutToGoal";
import { CircularProgress } from "@mui/material";


//Catalogue to for workouts 
const Workouts = () => {

    const data = workoutFuncs.FetchWorkouts();



    if (data === undefined) {
        return <>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '80vh' }}
            >
                <CircularProgress />
            </Grid>
        </>
    }

    return (
        <>
            <h1>Workouts</h1>
            <Grid container spacing={2} >
                {data.map((workout, index) => (
                    <Grid item xs={4} mb={4} key={index}>
                        <Box>
                            <DisplayCard element={workout} id={index} type="workout" />
                        </Box>

                    </Grid>
                ))}
            </Grid>
        </>
    )
}

export default Workouts;