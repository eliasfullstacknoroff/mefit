import '../App.css';
import { Grid, Typography, CircularProgress, Card, Box } from "@mui/material";
import programFuncs from "../utils/TrainingProgramContext"
import workoutFuncs from "../utils/WorkoutContext"
import DisplayCard from "../components/DisplayCard/DisplayCard";

//Asd

const Programpage = () => {

    //Workout id of the selected program
    const programId = window.location.pathname.match(/\d+/)[0];
    //Data for the selected program 
    const data = programFuncs.FetchTrainingProgram(programId);

    //Data for the exercises in the selected workout
    const workoutsData = programFuncs.FetchWorkoutsInProgram(programId);
    //List to hold specific data for the workout




    if (data === undefined) {
        return <>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '80vh' }}
            >
                <CircularProgress />
            </Grid>
            <>Fetching training programs...</>
        </>
    }
    if (workoutsData === undefined) {
        return <>
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
                style={{ minHeight: '80vh' }}
            >
                <CircularProgress />
            </Grid>
            <>Fetching training programs...</>
        </>
    }

    return (
        <>
            <h1>{data.name}</h1>
            <img src={data.image} alt="program.pic" />

            <Box sx={{ alignItems: 'center', mb: '2rem', mt: '1rem' }}>
                <Typography variant="h5">Category: {data.category}</Typography>
            </Box>

            <Card>
                <Typography marginBottom={2} variant="h4">Workouts in this program:</Typography>


                <Grid container spacing={2} >

                    {workoutsData.map((workout, index) => (
                        <Grid item xs={4} mb={4} key={index}>
                            <DisplayCard element={workout} id={index} type="workout" />
                        </Grid>
                    ))}
                </Grid>
            </Card>
        </>

    )
}
export default Programpage;