import DisplayCard from "../components/DisplayCard/DisplayCard";
import AddExerciseToWorkoutCard from "../components/DisplayCard/AddExerciseToWorkoutCard";
import "../App.css";
import {
  Grid,
  CircularProgress,
  Box,
  Button,
  CardActionArea,
  Card,
  Typography
} from "@mui/material";
import exerciseFuncs from "../utils/ExerciseContext";
import { NavLink } from "react-router-dom";
import workoutFuncs from "../utils/WorkoutContext";
import { useState, useEffect } from "react";

//Asd

//Catalogue of all exercises in database
const ExercisesInWorkout = () => {
  //Fetch for all exercises
  const data = exerciseFuncs.FetchExercises();
  const workoutsData = workoutFuncs.FetchWorkouts();
  const [listExNames, setListExNames] = useState([]);

  const [listOfIds, setListOfIds] = useState([]);

  function handleClick() {
    //add exercises to id 10
    workoutFuncs.AddExerciseInWorkout(10, listOfIds);
  }

  //Check if data is received from database
  if (workoutsData === undefined) {
    return (
      <>
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: "80vh" }}
        >
          <CircularProgress />
        </Grid>
      </>
    );
  }

  //Check if data is received from database
  if (data === undefined) {
    return (
      <>
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justifyContent="center"
          style={{ minHeight: "80vh" }}
        >
          <CircularProgress />
        </Grid>
      </>
    );
  }
  const listItems = listExNames.map((name) => <li>{name}</li>);

  return (
    <>
      <h1>Choose exercises to add</h1>
      <Card sx={{ alignItems: "center", mb: "1rem", mt: "0.5rem", position: "fixed", left: 10, zIndex: 2, width: 200 }}
      >
        <Typography variant="h5" sx={{ textAlign: 'center' }}>Exercises added </Typography>
        <ul>{listItems}</ul>
      </Card>

      <Button
        component={NavLink}
        to="/workouts"
        sx={{
          alignItems: "center",
          mb: "1rem",
          mt: "0.5rem",
          position: "fixed",
          bottom: 55,
          zIndex: 2,
          left: 10
        }}
        variant="contained"
        onClick={handleClick}
      >
        Add selected
      </Button>

      <Grid container spacing={2}>
        {data.map((exercise, index) => (
          <Grid item xs={4} mb={4} key={index} >
              <CardActionArea sx={{ maxWidth: 360, maxHeight: 360 }}
              onClick={() => {
                listOfIds.push(exercise.id);

                const index = listExNames.indexOf(exercise.name);
                if (index !== -1) {
                  setListExNames(listExNames => listExNames.filter((name) => (name != exercise.name)));
                }
                else {
                  setListExNames(listExNames => [...listExNames, exercise.name]);

                };
              }
              }
            >

              <AddExerciseToWorkoutCard
                element={exercise}
                id={index}
                type="exercise"
              > </AddExerciseToWorkoutCard>
            </CardActionArea>
          </Grid>

        ))}
      </Grid>
    </>
  );
};

export default ExercisesInWorkout;
